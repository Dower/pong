
function gameLoop(){
	
//=== Player1 movement ===\\ 
    if (up && y>0) y = y - screenHeight/80
    if (down && y<screenHeight-platform_height) y = y + screenHeight/80
    document.querySelector('.p1').style.top = y+'px'
	
//=== Player2 movement ===\\ 
    if (up2 && y2>0) y2 = y2 - screenHeight/80
    if (down2 && y2<screenHeight-platform_height) y2 = y2 + screenHeight/80
    document.querySelector('.p2').style.top = y2+'px'
	
//=== wall bounce ===\\ 
	if (ball_y<0){ball_dirrectionY=1;}
	if (ball_y>screenHeight-ball_size){ball_dirrectionY=2;}
	
//=== platform bounce ===\\ 
	if (ball_x<0+platform_width && ball_y>y-ball_size && ball_y<y+platform_height){
		ball_dirrectionX=1; 
		time++;
	}
	if (ball_x>screenWidth-platform_width-ball_size && ball_y>y2-ball_size && ball_y<y2+platform_height){
		ball_dirrectionX=2;
		time++;
	}
	
//=== Out of bounds ===\\ 
	if (ball_x<0-platform_width) {
		p2_points++;
		document.querySelector('.sp2').innerHTML = p2_points;
		console.log(p1_points+" : "+p2_points); 
		ball_reset();
		time=0;
		speed_increase=0;
	}
	if (ball_x>screenWidth+platform_width-ball_size) {
		p1_points++;
		document.querySelector('.sp1').innerHTML = p1_points;
		console.log(p1_points+" : "+p2_points);
		ball_reset();
		time=0;
		speed_increase=0;
	}
	
//=== speed-up ===\\ 
	if (time==5) {
		speed_increase +=ball_speed/10; 
		time=0; 
		console.log("Speed: "+speed_increase);
	}
	
//=== Ball movement ===\\ 
	if (ball_dirrectionX==1) ball_x = ball_x+ball_speed +speed_increase
	if (ball_dirrectionX==2) ball_x = ball_x-ball_speed -speed_increase
	if (ball_dirrectionY==1) ball_y = ball_y+ball_speed +speed_increase
	if (ball_dirrectionY==2) ball_y = ball_y-ball_speed -speed_increase
    document.querySelector('.ball').style.top = ball_y+'px'
    document.querySelector('.ball').style.left = ball_x+'px'
	
  window.requestAnimationFrame(gameLoop)
}
window.requestAnimationFrame(gameLoop)